package com.sanggil.javabasic1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaBasic1Application {

    public static void main(String[] args) {
        SpringApplication.run(JavaBasic1Application.class, args);
    }

}
